"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

/**
 * A schema for roads
 * @see {@link https://adonisjs.com/docs/4.0/migrations#_defining_schema}
 */
class RoadSchema extends Schema {
  /**
   * The `up` method is used to take action on a table.
   * It can be creating a new table or altering the existing table.
   */
  up() {
    this.create("roads", (table) => {
      table.increments();
      table.integer("speed_limit");
      table.timestamps();
    });
  }

  /**
   * The `down` method is the reverse of the up action.
   * When `up` method creates a table, you simply drop it inside the `down` method.
   * @returns {void}
   */
  down() {
    this.drop("roads");
  }
}

module.exports = RoadSchema;
