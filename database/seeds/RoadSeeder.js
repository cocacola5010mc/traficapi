"use strict";

/*
|--------------------------------------------------------------------------
| RoadSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use("Factory");

class RoadSeeder {
  run = async () => {
    const { speedLimits } = await import("./_data.mjs");
    const roads = []; // Avoid awaiting in loops

    Factory.blueprint("App/Models/Road", (_, __, { speed_limit }) => ({
      speed_limit,
    }));

    for (const speedLimit of speedLimits) {
      roads.push(
        Factory
          .model("App/Models/Road")
          .create({ speed_limit: speedLimit }),
      );
    }

    await Promise.all(roads);
  };
}

module.exports = RoadSeeder;
