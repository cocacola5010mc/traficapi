"use strict";

/*
|--------------------------------------------------------------------------
| CarSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const randInt = (min, max) => Math.round(Math.random() * (max - min) + min);

function* randomSpeeds(roadSpeed) {
  for (let i = 0; i < randInt(5, 10); i++) {
    const res = randInt(0, 20);

    /**
     * If res was smaller than 1, yield a speed less than the speedlimit
     * Otherswise, yeild a speed the same or highter than the speed limit
     */
    yield res > 1 ? roadSpeed + randInt(0, 10) : roadSpeed - randInt(1, 5);
  }
}

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use("Factory");

class CarSeeder {
  run = async () => {
    const { roads } = await import("./_data.mjs");
    const cars = [];

    Factory.blueprint("App/Models/Car", (_, __, { speed, road_id }) => ({
      speed,
      road_id,
    }));

    for (const [id, roadSpeed] of Object.entries(roads)) {
      for (const speed of randomSpeeds(roadSpeed)) {
        cars.push(
          Factory
            .model("App/Models/Car")
            .create({ speed, road_id: id }),
        );
      }
    }
  };
}

module.exports = CarSeeder;
