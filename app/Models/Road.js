"use strict";

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");

/**
 * A lucid model for a road
 * @see {@link https://adonisjs.com/docs/4.0/lucid}
 * @returns {void}
 */
class Road extends Model {
  /**
   * Gets all cars in App/Models/Car within this specific Road
   * @returns {Promise.<Object.<string, Array.<Object>>>}
   */
  cars = () => this.hasMany("App/Models/Car").fetch();
}

module.exports = Road;
