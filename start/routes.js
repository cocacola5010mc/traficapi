"use strict";

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use("Route");

Route.on("/").render("welcome");

// Gets data from a TrafficCOntroller method
Route.get("/hasTraffic/:id", "TrafficController.hasTraffic");
Route.get("/index/:id", "TrafficController.indexRoad");
Route.get("/allRoads", "TrafficController.getRoads");
Route.get("/allCars", "TrafficController.getCars");

// Calls method in TrafficController on post
Route.post("/newCar", "TrafficController.newCar");
Route.post("/newRoad", "TrafficController.newRoad");
